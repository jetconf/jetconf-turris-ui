import React from 'react';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import RouterList from './routes/RouterList';
import RouterScreen from './routes/RouterScreen';
import NotFound from './routes/NotFound';

const RedirectToList = () => <Redirect to="/list/select" />;

const App = () => (
  <Router>
    <div>
      <Switch>
        <Route exact path="/" component={RedirectToList} />
        <Route path="/list" component={RouterList} />
        <Route path="/:routerUrl/:configModule" component={RouterScreen} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </Router>
);

export default App;
