/* eslint-disable jsx-a11y/click-events-have-key-events */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GenericConfig from './GenericConfig';

const statusBg = { tagged: '#36a1eb', untagged: '#ecb961', off: '#dbdbdb' };

const PortContainer = styled.div`
  display: flex;
  flex-direction: column;
  text-align: left;
  width: 8em;
  border-left: 0.5em solid ${props => statusBg[props.status]};

  input {
    width: 1.25em;
    height: 1.25em;
    cursor: pointer;
  }

  div {
    padding: 0.5em 1em;
    height: 2.5em;

    &:not(:last-child) {
      border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    }
  }

  h4 {
    padding: 0.5em 1em;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  }

  &:first-child {
    max-width: none;
    text-align: left;
    border: 0;
  }

  &:not(:first-child) {
    div {
      cursor: pointer;
    }
  }
`;

const VlanContainer = styled.div`
  margin-bottom: 1em;
`;

const Ports = styled.div`
  display: flex;
  margin: 1em 0 2em;
  padding: 1em;
  box-shadow: 0 0.1em 0.4em rgba(0, 0, 0, 0.3);
`;

const Port = ({
  number, status, vlanId, changePortStatus
}) => (
  <PortContainer status={status}>
    <h4>{number}</h4>
    <div tabIndex={0} role="button" onClick={() => changePortStatus(number, vlanId, 'tagged')}>
      <input
        type="radio"
        checked={status === 'tagged'}
        onChange={() => changePortStatus(number, vlanId, 'tagged')}
      />
    </div>
    <div tabIndex={0} role="button" onClick={() => changePortStatus(number, vlanId, 'untagged')}>
      <input
        type="radio"
        checked={status === 'untagged'}
        onChange={() => changePortStatus(number, vlanId, 'untagged')}
      />
    </div>
    <div tabIndex={0} role="button" onClick={() => changePortStatus(number, vlanId, 'off')}>
      <input
        type="radio"
        checked={status === 'off'}
        onChange={() => changePortStatus(number, vlanId, 'off')}
      />
    </div>
  </PortContainer>
);

const Vlan = props => (
  <VlanContainer>
    <h2>VLAN {props.vid}</h2>
    <Ports>
      <PortContainer>
        <h4>Port</h4>
        <div>Tagged</div>
        <div>Untagged</div>
        <div>Off</div>
      </PortContainer>
      {props['all-ports'].map(portNumber => (
        <Port
          key={portNumber}
          number={portNumber}
          vlanId={props.vid}
          changePortStatus={props.changePortStatus}
          status={
            (props['tagged-ports'].has(portNumber) && 'tagged') ||
            (props['untagged-ports'].has(portNumber) && 'untagged') ||
            'off'
          }
        >
          {portNumber}
        </Port>
      ))}
    </Ports>
  </VlanContainer>
);

class Vlans extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        ...props.data,
        vlans: props.data.vlans.map(vlan => ({
          ...vlan,
          'tagged-ports': new Set(vlan['tagged-ports']),
          'untagged-ports': new Set(vlan['untagged-ports'])
        }))
      }
    };
    this.changePortStatus = this.changePortStatus.bind(this);
  }

  changePortStatus(portNumber, vlanId, newStatus) {
    const { vlans } = this.state.data;
    vlans.forEach((vlan) => {
      if (vlan.vid === vlanId) {
        switch (newStatus) {
          case 'tagged': {
            vlan['tagged-ports'].add(portNumber);
            vlan['untagged-ports'].delete(portNumber);
            break;
          }
          case 'untagged': {
            vlan['untagged-ports'].add(portNumber);
            vlan['tagged-ports'].delete(portNumber);
            break;
          }
          case 'off': {
            vlan['untagged-ports'].delete(portNumber);
            vlan['tagged-ports'].delete(portNumber);
            break;
          }
          default: {
            break;
          }
        }
      }
    });
    this.setState({
      data: { ...this.state.data, vlans }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state.data.vlans.map(vlan => (
          <Vlan
            {...vlan}
            changePortStatus={this.changePortStatus}
            all-ports={this.props.data.ports.map(port => port.number).sort()}
            key={vlan.vid}
          />
        ))}
        <GenericConfig data={this.state.data.vlans} />
        <GenericConfig data={this.props.data} />
      </React.Fragment>
    );
  }
}

Port.propTypes = {
  number: PropTypes.number.isRequired,
  vlanId: PropTypes.number.isRequired,
  status: PropTypes.string,
  changePortStatus: PropTypes.func
};

Vlan.propTypes = {
  vid: PropTypes.number,
  'tagged-ports': PropTypes.instanceOf(Set),
  'untagged-ports': PropTypes.instanceOf(Set),
  'all-ports': PropTypes.arrayOf(PropTypes.number),
  changePortStatus: PropTypes.func
};

Vlans.propTypes = {
  data: PropTypes.shape({
    vlans: PropTypes.arrayOf(
      PropTypes.shape({
        vid: PropTypes.number,
        'tagged-ports': PropTypes.arrayOf(PropTypes.number),
        'untagged-ports': PropTypes.arrayOf(PropTypes.number)
      })
    ),
    ports: PropTypes.arrayOf(
      PropTypes.shape({
        number: PropTypes.number
      })
    )
  })
};

export default Vlans;
