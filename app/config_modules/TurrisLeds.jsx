import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import colorToHex from '../utils/colorToHex';
import TurrisFront from '../assets/router/turris-1-leds.svg';
import OmniaFront from '../assets/router/turris-omnia-leds.svg';
import config from '../../config.json';
import GenericConfig from './GenericConfig';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .router-image {
    width: 60em;
    height: 15.5em;
    flex-shrink: 0;
    mask-image: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 1) 35%);
  }
`;

const Controls = styled.div`
  margin-top: 0.75em;
  display: flex;
  width: 52em;
  min-height: 5em;
`;

const Text = styled.p`
  text-align: center;
  font-weight: ${props => (props.bold ? 'bold' : 'normal')};
  color: inherit;
`;

const offsets = model =>
  (model === 'turris'
    ? {
      // turris
      wan: '27%',
      lan: '7.5%',
      wifi: '7.5%',
      pwr: '8%'
    }
    : {
      // omnia
      pwr: '3.5%',
      lan0: '0.8%',
      lan1: '0.8%',
      lan2: '1.2%',
      lan3: '1.2%',
      lan4: '0%',
      wan: '0%',
      pci1: '0.6%',
      pci2: '0.4%',
      pci3: '0.4%',
      usr1: '1.2%',
      usr2: '0.8%'
    });

const Led = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0.25em;
  margin-left: ${props => offsets(props.model)[props.name]};

  select {
    padding: 0.25em;
  }

  input[type='color'] {
    border: 0.15em solid #333;
    display: block;
    width: 2.5em;
    height: 2.5em;
    appearance: none;
    flex-shrink: 0;
    cursor: pointer;
    border-radius: 50%;
    background: transparent;
    margin-bottom: 0.5em;

    &::-webkit-color-swatch-wrapper {
      padding: 0;
      border-radius: 50%;
    }

    &::-webkit-color-swatch {
      border: 0;
      border-radius: 50%;
    }
  }
`;

const Intensity = styled.div`
  margin: 2em 0 0;
  min-height: 5em;

  input[type='range'] {
    vertical-align: middle;
    margin-right: 1em;
    width: 15em;
  }

  span {
    width: 3em;
    display: inline-block;
    text-align: left;
  }
`;

class TurrisLeds extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        ...props.data,
        leds: props.data.leds
          .sort((a, b) => {
            const order = props.routerModel.ledOrder;
            const aName = a.name.split(':')[1];
            const bName = b.name.split(':')[1];
            return order.indexOf(aName) - order.indexOf(bName);
          })
          .map(led => ({
            ...led,
            color: colorToHex(led.color)
          }))
      }
    };
    this.updateLedColor = this.updateLedColor.bind(this);
    this.updateLedStatus = this.updateLedStatus.bind(this);
    this.updateIntensity = this.updateIntensity.bind(this);
  }

  updateLedColor(event) {
    const { name, value } = event.target;

    this.setState({
      data: {
        ...this.state.data,
        leds: this.state.data.leds.map(led => ({
          ...led,
          color: led.name === name ? value : led.color
        }))
      }
    });
  }

  updateLedStatus(event) {
    const { name, value } = event.target;

    this.setState({
      data: {
        ...this.state.data,
        leds: this.state.data.leds.map(led => ({
          ...led,
          status: led.name === name ? value : led.status
        }))
      }
    });
  }

  updateIntensity(event) {
    const { value } = event.target;
    this.setState({
      data: {
        ...this.state.data,
        intensity: parseInt(value, 10)
      }
    });
  }

  render() {
    return (
      <Container>
        {this.props.routerModel.model === 'turris' ? <TurrisFront /> : <OmniaFront />}
        <Controls>
          {this.state.data.leds.map(led => (
            <Led
              key={led.name.split(':')[1]}
              name={led.name.split(':')[1]}
              model={this.props.routerModel.model}
            >
              <input
                type="color"
                name={led.name}
                onChange={this.updateLedColor}
                value={led.color}
              />
              <div>
                <Text bold>{config.text.ledNames[led.name]}</Text>
                <select name={led.name} value={led.status} onChange={this.updateLedStatus}>
                  <option value="auto">auto</option>
                  <option value="on">on</option>
                  <option value="off">off</option>
                </select>
              </div>
            </Led>
          ))}
        </Controls>
        <Intensity>
          <p>intensity:</p>
          <input
            type="range"
            max={100}
            min={0}
            onChange={this.updateIntensity}
            value={this.state.data.intensity}
          />
          <span>{this.state.data.intensity} %</span>
        </Intensity>
        <GenericConfig data={this.state.data.leds} />
      </Container>
    );
  }
}

TurrisLeds.propTypes = {
  data: PropTypes.shape({
    intensity: PropTypes.number,
    leds: PropTypes.arrayOf(
      PropTypes.shape({
        color: PropTypes.string,
        name: PropTypes.string,
        status: PropTypes.string
      })
    )
  }),
  routerModel: PropTypes.shape({
    model: PropTypes.string.isRequired,
    ledOrder: PropTypes.arrayOf(PropTypes.string).isRequired
  }).isRequired
};

export default TurrisLeds;
