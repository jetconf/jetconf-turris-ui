import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Code = styled.pre`
  font: 14px/1.4em Iosevka, Consolas, Inconsolata, Monaco, monospace;
  background: #333;
  color: #eee;
  padding: 1em;
  width: 100%;

  &:not(:first-child) {
    margin-top: 2em;
  }
`;

const GenericConfig = props => <Code>{JSON.stringify(props.data, null, 2)}</Code>;

GenericConfig.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
};

export default GenericConfig;
