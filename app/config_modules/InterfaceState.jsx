import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import IpIcon from '../assets/icons/ip.svg';
import TurrisBack from '../assets/router/turris-1-back.svg';
import OmniaBack from '../assets/router/turris-omnia-back.svg';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .router-image {
    width: 45em;
    height: 15.5em;
    flex-shrink: 0;
  }
`;

const Interfaces = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
`;

const Iface = styled.div`
  align-self: flex-start;
  padding: 1em;
  box-shadow: 0 0.1em 0.4em rgba(0, 0, 0, 0.3);
  margin: 0.5em;
  min-height: 9em;
  min-width: 16em;
  max-width: calc(25% - 1em);
  flex: 1;
  border-left: 0.5em solid ${props => (props['oper-status'] === 'up' ? '#00ff4f' : '#cccccc')};
`;

const IfaceName = styled.h2`
  font-size: 1em;
`;

const Meta = styled.div`
  font-size: 0.8em;
  display: flex;
  color: #666;
  word-wrap: break-word;
  margin-bottom: 0.5em;
  overflow: hidden;
  text-overflow: ellipsis;

  input {
    flex: 1;
    min-width: 0;
    border: 1px solid #ccc;
    border-right: 0;
    padding: 0.25em;

    &:last-child {
      border-right: 1px solid #ccc;
      border-left: 0;
      flex: 0;
      min-width: 2.5em;
    }
  }

  svg {
    display: inline;
    vertical-align: middle;
    height: 1.2em;
    margin: 0.3em 0.5em 0 0;

    path {
      fill: currentColor;
    }
  }
`;

const Divider = styled.hr`
  border-bottom: 0.05em solid #eee;
  margin-bottom: 1.4em;
`;

const typeMap = {
  'iana-if-type:ethernetCsmacd': 'Ethernet',
  'cznic-bridge-interface:bridge': 'Bridge'
};

const Interface = props => (
  <Iface {...props}>
    <IfaceName>
      {props.name} [{props['oper-status']}]
    </IfaceName>
    <Meta>
      {props['phys-address']}
      {props['phys-address'] && props.type ? ', ' : null}
      {typeMap[props.type]}
    </Meta>
    {props['ietf-ip:ipv4'] || props['ietf-ip:ipv6'] ? <Divider /> : null}
    {props['ietf-ip:ipv4']
      ? props['ietf-ip:ipv4'].address.map(ip => (
        <Meta key={ip.ip}>
          <IpIcon /> <input defaultValue={ip.ip} />/<input defaultValue={ip['prefix-length']} />
        </Meta>
        ))
      : null}
    {props['ietf-ip:ipv6']
      ? props['ietf-ip:ipv6'].address.map(ip => (
        <Meta key={ip.ip}>
          <IpIcon /> <input defaultValue={ip.ip} />/<input defaultValue={ip['prefix-length']} />
        </Meta>
        ))
      : null}
  </Iface>
);

Interface.propTypes = {
  name: PropTypes.string.isRequired,
  'oper-status': PropTypes.string.isRequired,
  'phys-address': PropTypes.string,
  type: PropTypes.string.isRequired,
  'ietf-ip:ipv4': PropTypes.shape({
    address: PropTypes.arrayOf(
      PropTypes.shape({
        'prefix-length': PropTypes.number.isRequired,
        ip: PropTypes.string.isRequired
      })
    )
  }),
  'ietf-ip:ipv6': PropTypes.shape({
    address: PropTypes.arrayOf(
      PropTypes.shape({
        'prefix-length': PropTypes.number.isRequired,
        ip: PropTypes.string.isRequired
      })
    )
  })
};

const InterfaceState = props => (
  <Container>
    {props.routerModel.model === 'turris' ? <TurrisBack /> : <OmniaBack />}
    <Interfaces>
      {props.data.interface.map(iface => <Interface key={iface.name} {...iface} />)}
    </Interfaces>
  </Container>
);

InterfaceState.propTypes = {
  data: PropTypes.shape({
    interface: PropTypes.arrayOf(PropTypes.shape(Interface.propTypes))
  }).isRequired,
  routerModel: PropTypes.shape({
    model: PropTypes.string.isRequired
  }).isRequired
};

export default InterfaceState;
