import React from 'react';
import { Link } from 'react-router-dom';
import NotFound from './NotFound';
import RouterCard from '../RouterCard';
import Button from '../layout/Button';
import db from '../utils/db';

class RouterListSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      routers: []
    };
  }

  componentWillMount() {
    this.loadRouters();
  }

  loadRouters() {
    db
      .table('routers')
      .toArray()
      .then((routers) => {
        this.setState({ routers });
      });
  }

  render() {
    return (
      <div>
        {this.state.routers.length < 1 ? (
          <NotFound
            text="No routers configured."
            buttons={[
              <Button key="btn-add">
                <Link to="/list/add">Add a new router</Link>
              </Button>
            ]}
          />
        ) : null}
        {this.state.routers.map(router => (
          <RouterCard
            key={router.name}
            title={router.name}
            url={router.url}
            color={router.color}
            note={router.note}
            location={router.location}
          />
        ))}
      </div>
    );
  }
}

export default RouterListSelect;
