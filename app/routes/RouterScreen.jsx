import React from 'react';
import PropTypes from 'prop-types';
import { Link, NavLink } from 'react-router-dom';
import Button from '../layout/Button';
import CommitIcon from '../assets/icons/commit.svg';
import HelpIcon from '../assets/icons/help.svg';
import BackIcon from '../assets/icons/back.svg';
import ResetIcon from '../assets/icons/reset.svg';
import Sidebar from '../layout/Sidebar';
import SidebarMenuItem from '../layout/SidebarMenuItem';
import SidebarSpacer from '../layout/SidebarSpacer';
import AppContainer from '../layout/AppContainer';
import NotFound from './NotFound';
import AppContent from '../layout/AppContent';
import MainArea from '../layout/MainArea';
import config from '../../config.json';
import Header from '../layout/Header';
import TurrisLeds from '../config_modules/TurrisLeds';
import InterfaceState from '../config_modules/InterfaceState';
import Vlans from '../config_modules/Vlans';
import GenericConfig from '../config_modules/GenericConfig';
import db from '../utils/db';
import modelFromCPU from '../utils/modelFromCPU';

class RouterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      routerConfig: null,
      routerModel: null,
      loadingData: true
    };

    this.resetData = this.resetData.bind(this);
  }

  componentDidMount() {
    this.loadRouterConfig(this.props.match.params.routerUrl);
    this.loadData(this.props.match.params.routerUrl);
  }

  componentMap(configModule) {
    const map = {
      'ietf-interfaces:interfaces-state': (
        <InterfaceState
          data={this.state.data ? this.state.data['ietf-interfaces:interfaces-state'] : null}
          routerModel={this.state.routerModel || null}
        />
      ),
      'cznic-rainbow-common:turris-leds': (
        <TurrisLeds
          data={this.state.data ? this.state.data['cznic-rainbow-common:turris-leds'] : null}
          routerModel={this.state.routerModel || null}
        />
      ),
      'cznic-ethernet-switch:switch': (
        <GenericConfig
          data={this.state.data ? this.state.data['cznic-ethernet-switch:switch'] : null}
          routerModel={this.state.routerModel || null}
        />
      ),
      'ietf-interfaces:interfaces': (
        <GenericConfig
          data={this.state.data ? this.state.data['ietf-interfaces:interfaces'] : null}
          routerModel={this.state.routerModel || null}
        />
      ),
      'cznic-ethernet-switch:ports': (
        <GenericConfig
          data={this.state.data ? this.state.data['cznic-ethernet-switch:ports'] : null}
          routerModel={this.state.routerModel || null}
        />
      ),
      'cznic-ethernet-switch:vlans': (
        <Vlans
          data={
            this.state.data
              ? {
                  vlans: this.state.data['cznic-ethernet-switch:vlans'],
                  ports: this.state.data['cznic-ethernet-switch:ports']
                }
              : null
          }
          routerModel={this.state.routerModel || null}
        />
      )
    };
    return map[configModule];
  }

  loadRouterConfig(url) {
    db.routers.get(url).then((routerConfig) => {
      this.setState({ routerConfig });
      document.title = `${routerConfig.name} (${routerConfig.url})`;
    });
  }

  loadData(url) {
    this.setState({
      loadingData: true
    });
    fetch(`https://${url}/restconf/data`)
      .then((response) => {
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/yang.api+json')) {
          return response.json();
        }
        throw new TypeError(`Got '${contentType}' instead of application/yang.api+json.`);
      })
      .then((json) => {
        this.setState({
          data: json['ietf-restconf:data'],
          routerModel: modelFromCPU(json['ietf-restconf:data']),
          loadingData: false
        });
      })
      .catch(() => {
        this.setState({
          loadingData: false
        });
      });
  }

  resetData() {
    // eslint-disable-next-line no-alert
    if (window.confirm('Do you really want to perform reset? Uncommited changes will be lost.')) {
      this.loadData(this.props.match.params.routerUrl);
    }
  }

  render() {
    return (
      <AppContainer>
        <Header
          title={
            this.state.routerConfig
              ? `${this.state.routerConfig.name} (${this.state.routerConfig.url})`
              : ''
          }
          buttons={[
            <Button onClick={this.resetData} background="#c14529" key="btn-reset">
              <ResetIcon /> Reset
            </Button>,
            <Button key="btn-commit">
              <CommitIcon /> Commit
            </Button>
          ]}
        />
        <AppContent>
          <Sidebar>
            {this.state.data && this.state.routerConfig
              ? Object.keys(this.state.data).map(
                  item =>
                (config.text.modules[item] ? (
                  <SidebarMenuItem key={item}>
                    <NavLink
                      exact
                      to={`/${this.state.routerConfig.url}/${item}`}
                      isActive={() => item === this.props.match.params.configModule}
                    >
                      {config.text.modules[item]}
                    </NavLink>
                  </SidebarMenuItem>
                ) : null)
              )
              : null}
            <SidebarSpacer />
            <SidebarMenuItem borderTop>
              <NavLink exact to="/docs">
                <HelpIcon /> Docs
              </NavLink>
            </SidebarMenuItem>
            <SidebarMenuItem>
              <Link to="/">
                <BackIcon /> Back to router list
              </Link>
            </SidebarMenuItem>
          </Sidebar>
          <MainArea>
            {!this.state.loadingData && this.state.data ? (
              this.componentMap(this.props.match.params.configModule)
            ) : (
              <NotFound
                text={
                  this.state.loadingData
                    ? 'Loading data from Jetconf…'
                    : `An error ocurred while fetching data from ${
                        this.props.match.params.routerUrl
                      }.`
                }
              />
            )}
          </MainArea>
        </AppContent>
      </AppContainer>
    );
  }
}

RouterScreen.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      routerUrl: PropTypes.string.isRequired,
      configModule: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

export default RouterScreen;
