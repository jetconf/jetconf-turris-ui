import React from 'react';
import styled from 'styled-components';
import { Redirect } from 'react-router-dom';
import db from '../utils/db';
import Button from '../layout/Button';
import SaveIcon from '../assets/icons/save.svg';

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  input[type='text'],
  textarea {
    border: 0.05em solid #ccc;
    border-radius: 0.25em;
    margin-bottom: 1em;
    padding: 0.5em;
    font: 1rem/1.4em sans-serif;
    width: 100%;
  }

  input[type='color'] {
    height: 3em;
    width: 3em;
    margin-bottom: 1em;
    cursor: pointer;
  }
`;

class RouterListAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonDisabled: true,
      redirect: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    });
    this.setState({
      buttonDisabled: !(
        this.state.name &&
        this.state.url &&
        (this.state.name.length > 0 || this.state.url.length > 0)
      )
    });
  }

  handleSubmit() {
    const router = {
      name: this.state.name,
      url: this.state.url,
      location: this.state.location,
      color: this.state.color,
      note: this.state.note
    };
    db
      .table('routers')
      .add(router)
      .then(() => {
        this.setState({
          redirect: true
        });
      });
  }

  render() {
    return this.state.redirect ? (
      <Redirect push to="/list/select" />
    ) : (
      <Form>
        <input type="text" placeholder="Name" id="name" onChange={this.handleChange} />
        <input type="text" placeholder="Host (ip:port)" id="url" onChange={this.handleChange} />
        <input
          type="text"
          placeholder="Physical location"
          id="location"
          onChange={this.handleChange}
        />
        <input id="color" type="color" onChange={this.handleChange} />
        <textarea placeholder="Notes" id="note" onChange={this.handleChange} />
        <Button onClick={this.handleSubmit} disabled={this.state.buttonDisabled}>
          <SaveIcon /> Save
        </Button>
      </Form>
    );
  }
}

export default RouterListAdd;
