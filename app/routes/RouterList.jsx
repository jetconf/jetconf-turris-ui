import React from 'react';
import { Switch, Route, Link, NavLink } from 'react-router-dom';
import config from '../../config.json';
import Header from '../layout/Header';
import AppContainer from '../layout/AppContainer';
import AppContent from '../layout/AppContent';
import MainArea from '../layout/MainArea';
import Button from '../layout/Button';
import NotFound from './NotFound';
import Sidebar from '../layout/Sidebar';
import SidebarMenuItem from '../layout/SidebarMenuItem';
import SidebarSpacer from '../layout/SidebarSpacer';
import RouterListSelect from './RouterListSelect';
import RouterListAdd from './RouterListAdd';
import HelpIcon from '../assets/icons/help.svg';
import ManageIcon from '../assets/icons/pencil.svg';
import AddIcon from '../assets/icons/plus.svg';
import ListIcon from '../assets/icons/list.svg';
import ExportIcon from '../assets/icons/export.svg';
import ImportIcon from '../assets/icons/import.svg';

const RouterList = () => (
  <AppContainer>
    <Header
      title={config.text.title}
      buttons={[
        <Button key="btn-add">
          <Link to="/list/add">
            <AddIcon /> Add a router
          </Link>
        </Button>
      ]}
    />
    <AppContent>
      <Sidebar>
        <SidebarMenuItem>
          <NavLink exact to="/list/select">
            <ListIcon /> Router list
          </NavLink>
        </SidebarMenuItem>
        <SidebarMenuItem>
          <NavLink exact to="/list/manage">
            <ManageIcon /> Manage routers
          </NavLink>
        </SidebarMenuItem>
        <SidebarMenuItem>
          <NavLink exact to="/list/import">
            <ImportIcon /> Import config
          </NavLink>
        </SidebarMenuItem>
        <SidebarMenuItem>
          <NavLink exact to="/list/export">
            <ExportIcon /> Export config
          </NavLink>
        </SidebarMenuItem>
        <SidebarSpacer />
        <SidebarMenuItem borderTop>
          <NavLink exact to="/docs">
            <HelpIcon /> Docs
          </NavLink>
        </SidebarMenuItem>
      </Sidebar>
      <MainArea>
        <Switch>
          <Route exact path="/list/select" component={RouterListSelect} />
          <Route exact path="/list/add" component={RouterListAdd} />
          {/* <Route path="/list/import" component={RouterListImport} />
          <Route path="/list/export" component={RouterListExport} /> */}
          <Route default component={NotFound} />
        </Switch>
      </MainArea>
    </AppContent>
  </AppContainer>
);

export default RouterList;
