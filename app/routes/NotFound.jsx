import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';
import RouterIcon from '../assets/icons/router.svg';

const appear = keyframes`
  from {
    opacity: 0;
    transform: translate(0, -1em);
  }

  to {
    opacity: 1;
    transform: translate(0, 0);
  }
`;

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  flex-direction: column;
  animation: ${appear} 0.3s ease-in;
`;

const Text = styled.p`
  font-size: 1.4em;
  font-weight: bold;
  margin-bottom: 1em;
  text-align: center;
`;

const NotFound = props => (
  <Container>
    <RouterIcon style={{ width: '4em', height: '4em' }} />
    <Text>{props.text || 'not found :('}</Text>
    {props.buttons ? <div>{props.buttons}</div> : null}
  </Container>
);

NotFound.propTypes = {
  text: PropTypes.string,
  buttons: PropTypes.arrayOf(PropTypes.element)
};

export default NotFound;
