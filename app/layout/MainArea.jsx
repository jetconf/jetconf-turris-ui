import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 1em;
  overflow-y: auto;
  height: calc(100vh - 4em);
`;
