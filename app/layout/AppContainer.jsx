import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  background: #fefefe;
  color: #111;
  min-height: 100vh;
`;
