import styled from 'styled-components';
import color from 'color';

const defBg = 'rgb(58, 122, 166)';
const defFg = 'rgb(231, 242, 242)';

export default styled.button`
  font-size: 0.8em;
  display: flex;
  flex: 0;
  white-space: nowrap;
  justify-content: center;
  align-items: center;
  background: ${props => props.background || defBg};
  color: ${props => props.color || defFg};
  box-shadow: 0 0.1em 0 rgba(0, 0, 0, 0.3);
  text-transform: uppercase;
  padding: 0.5em 0.75em;
  border-radius: 0.15em;
  cursor: pointer;
  transition: background 0.33s, transform 0.1s;
  outline: none;
  font-weight: bold;
  height: 2.5em;
  margin: 0 0.25em;
  user-select: none;

  a {
    &,
    &:link,
    &:visited,
    &:hover,
    &:focus,
    &:active {
      display: flex;
      justify-content: center;
      align-items: center;
      text-decoration: none;
      margin: -0.5em -0.75em;
      color: ${props => props.color || defFg};
      background: transparent;
      padding: 0.5em 0.75em;
    }
  }

  &:hover,
  &:focus {
    background: ${props => color(props.background || defBg).lighten(0.3).saturate(0.4).string()};
    will-change: background;
  }

  &:active {
    will-change: background, box-shadow, transform;
    background: ${props => color(props.background || defBg).darken(0.1).string()};
    transform: translateY(0.1em);
    box-shadow: none;
  }

  &:disabled {
    color: ${props => color(props.background || defBg).lighten(0.3).string()};
    cursor: not-allowed;

    &:hover,
    &:focus {
      background: ${props => props.background || defBg};
    }
  }

  svg {
    height: 1.5em;
    margin-right: 0.4em;
    margin-left: -0.2em;

    path {
      fill: currentColor;
    }
  }
`;
