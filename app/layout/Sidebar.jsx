import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: auto;
  overflow-x: hidden;
  background: rgb(46, 57, 65);
  color: rgb(231, 242, 242);
  border-right: 0.1em solid rgb(43, 56, 65);
  width: 15em;
`;
