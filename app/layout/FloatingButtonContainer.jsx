import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: row;
  position: fixed;
  align-items: center;
  right: 1em;
  bottom: 1em;
`;
