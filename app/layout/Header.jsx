import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Container = styled.header`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: flex-start;
  align-items: center;
  background: rgb(51, 71, 85);
  height: 4em;
  padding: 0.5em;
  box-shadow: 0 0.1em 0.4em rgba(0, 0, 0, 0.3);
`;

const AppTitle = styled.h1`
  flex: 1;
  font-size: 1.4em;
  color: #fefefe;
  text-shadow: 0 0.1em 0 rgba(0, 0, 0, 0.3);
  white-space: nowrap;

  a {
    &,
    &:link,
    &:visited,
    &:hover,
    &:focus,
    &:active {
      padding-left: 0.2em;
      color: #fefefe;
      text-decoration: none;
    }
  }

  svg {
    margin-right: 0.25rem;
  }
`;

const ButtonContainer = styled.div`
  width: auto;
  display: flex;
`;

const AppHeader = props => (
  <Container>
    <AppTitle>
      <Link to="/">{props.title}</Link>
    </AppTitle>
    <ButtonContainer>{props.buttons}</ButtonContainer>
  </Container>
);

AppHeader.propTypes = {
  title: PropTypes.string.isRequired,
  buttons: PropTypes.arrayOf(PropTypes.element)
};

export default AppHeader;
