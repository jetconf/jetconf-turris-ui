import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: row;
  color: rgb(231, 242, 242);
  border-bottom: 1px solid rgb(35, 46, 54);
  border-top: ${props => (props.borderTop ? '1px solid rgb(35, 46, 54)' : 0)};

  a {
    &,
    &:link,
    &:visited,
    &:hover,
    &:focus,
    &:active {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      text-decoration: none;
      color: rgb(170, 185, 185);
      background: rgb(46, 57, 65);
      padding: 1em 0.5em;
      border-left: 0.3em solid rgb(51, 71, 85);
      width: 100%;
      transition: background 0.33s, border 0.33s;
    }

    &:hover,
    &:focus {
      color: rgb(231, 242, 242);
      background: rgb(34, 44, 51);
    }

    &.active {
      background: rgb(34, 44, 51);
      border-left: 0.3em solid rgb(58, 122, 166);
      color: rgb(231, 242, 242);
    }

    svg {
      height: 1.4em;
      margin-right: 0.5em;
      margin-top: -0.2em;

      path {
        fill: currentColor;
      }
    }
  }
`;
