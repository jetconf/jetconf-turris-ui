import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import config from '../config.json';
import LinkIcon from './assets/icons/link.svg';
import LocationIcon from './assets/icons/location.svg';

const Container = styled.div`
  box-shadow: 0 0.1em 0.4em rgba(0, 0, 0, 0.3);
  margin-bottom: 1em;
  border-left: 1.4em solid ${props => props.color || 'rgb(151, 164, 176)'};
  border-radius: 0.15em;
  transition: box-shadow 0.33s;

  &:hover {
    box-shadow: 0 0.2em 0.6em rgba(0, 0, 0, 0.4);
  }

  a {
    padding: 0.5em 1em 0.5em 0.5em;
    width: 100%;
    text-decoration: none;
    display: flex;
  }
`;

const RouterTitle = styled.h1`
  font-size: 1em;
  font-weight: bold;
  margin-bottom: 1.4em;
  color: #111;
  width: 16em;
`;

const RouterMeta = styled.p`
  color: #666;
  font-weight: ${props => (props.bold ? 'bold' : 'normal')};
  white-space: ${props => (props.oneLine ? 'nowrap' : 'inherit')};

  svg {
    display: inline;
    vertical-align: middle;
    height: 1.2em;
    margin-top: -0.25em;

    path {
      fill: currentColor;
    }
  }
`;

const RouterNote = styled.div`
  margin-left: 1em;
  padding-left: 0.5em;
  border-left: 0.05em solid #ddd;
`;

const RouterCard = props => (
  <Container color={props.color}>
    <Link to={`/${props.url}/${config.primaryModule}`}>
      <div>
        <RouterTitle>{props.title}</RouterTitle>
        <RouterMeta bold oneLine>
          <LinkIcon /> {props.url}
        </RouterMeta>
        {props.location ? (
          <RouterMeta oneLine>
            <LocationIcon /> {props.location}
          </RouterMeta>
        ) : null}
      </div>
      {props.note ? (
        <RouterNote>
          <RouterMeta>{props.note}</RouterMeta>
        </RouterNote>
      ) : null}
    </Link>
  </Container>
);

RouterCard.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  location: PropTypes.string,
  color: PropTypes.string,
  note: PropTypes.string
};

export default RouterCard;
