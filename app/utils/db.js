import Dexie from 'dexie';

const db = new Dexie('TurrisList');
db.version(1).stores({
  routers: '&url,name,location,color,note'
});

export default db;
