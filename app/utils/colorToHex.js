import color from 'color';

export default function colorToHex(colorDef) {
  return colorDef.match(/^[0-9A-F]{6}$/)
    ? color(`#${colorDef}`).hex()
    : color(colorDef).hex();
}
