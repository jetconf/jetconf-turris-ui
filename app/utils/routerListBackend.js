import config from '../../config.json';

const url = config.routerListBackend;

export default {
  router: {
    list: async () => fetch(`${url}/router/list`).then(r => r.json())
  },
  group: {
    list: async () => fetch(`${url}/group/list`).then(r => r.json())
  }
};
