import config from '../../config.json';

export default function modelFromCPU(data) {
  const cpuName = data['ietf-hardware-state:hardware'].component.filter(
    component => component.name === 'CPU'
  )[0]['model-name'];
  return config.routerModels.filter(model => model.cpuName === cpuName)[0];
}
